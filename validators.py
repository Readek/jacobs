import pyinputplus as pyip

def countryValidity(country):
    if country.upper() in ["CA", "CLP", "IE", "US", "UK", "PL", "IN", "QA", "AE", "DEL"]:
        return True
    print("You have entered incorrect country, try again.")
    return False


def dateValidity():
    return pyip.inputDate("Enter date in format DD-MM-YYYY: ", formats=["%d-%m-%Y"])


def timeValidity():
    return pyip.inputTime("Enter time in format HH:MM: ")
