import win32com.client as win32
import openpyxl
import os
import pandas
import numpy
import time
import datetime as dt

start = time.time()
today = dt.datetime.today()
yesterday = today - dt.timedelta(days=1)
lastWeekDateTime = yesterday - dt.timedelta(days=6)
dest_filename = f"Email_Report_{today.year}-{today.month}-{today.day}.xlsx"


mailboxes = [
    "AEAPInvoices",
    "QAAPInvoices",
    "IEAPInvoices",
    "PLAPInvoices",
    "DEAPInvoices",
    "UKAPInvoices",
    "CLAPInvoices",
    "CAAPInvoices",
    "USAPInvoices",
]
reportDirectory = "C:\\Users\\RG053306\\Documents\\Reports\\Email_Report"
os.chdir(reportDirectory)


def createFile(lastWeekDateTime, dest_filename):
    wb = openpyxl.Workbook()
    wb.active.title = "Summary"
    for mailbox in mailboxes:
        wb.create_sheet(title=f"{mailbox}")
    wb.save(filename=dest_filename)


def widthColumnAdjust():
    ws = wb["Summary"]
    for column in range(1, len(mailboxes) + 2):
        column_letter = openpyxl.utils.get_column_letter(column)
        ws.column_dimensions[column_letter].width = 15
    wb.save(filename=dest_filename)


def send(lastWeekDateTime, dest_filename):
    outlook = win32.Dispatch("outlook.application")
    mail = outlook.CreateItem(0)
    mailAddress = "Radoslaw.Gasior@jacobs.com; Justyna.Polak@jacobs.com; Katarzyna.Dabrowska@jacobs.com; Justyna.Straczek@jacobs.com; Beata.Gasior@jacobs.com"
    mail.To = mailAddress
    mail.Subject = f"Email report {today.year}-{today.month}-{today.day}"
    mail.Body = f'🌻Please find attached report with email count for mailboxes: {", ".join(mailbox for mailbox in mailboxes)}.🌻'
    attachment = os.path.join(reportDirectory, dest_filename)
    mail.Attachments.Add(attachment)
    mail.Send()


createFile(lastWeekDateTime, dest_filename)
wb = openpyxl.load_workbook(dest_filename)

outlook = win32.Dispatch("Outlook.Application").GetNamespace("MAPI")
for mailbox in mailboxes:
    ws = wb[f"{mailbox}"]
    ws["A1"] = "Mailbox"
    ws["B1"] = "Subject"
    ws["C1"] = "Date"
    ws["D1"] = "Sender"

    folder = outlook.Folders.Item(f"{mailbox}")
    inbox = folder.Folders.Item("Inbox")
    msg = inbox.Items
    lastWeekMessages = msg.Restrict(
        "[ReceivedTime] >= '"
        + lastWeekDateTime.strftime("%m/%d/%Y")
        + "' And [ReceivedTime] <= '"
        + today.strftime("%m/%d/%Y 00:00")
        + "'"
    )

    i = 2
    for item in lastWeekMessages:
        try:
            if item.Attachments:
                ws[f"A{i}"] = mailbox
                ws[f"B{i}"] = str(item.Subject)
                ws[f"C{i}"] = str(item.ReceivedTime)[:10]
                ws[f"D{i}"] = str(item.Sender)
                print(item.ReceivedTime)
            i += 1
        except Exception:
            pass

    wb.save(dest_filename)

emails = [
    pandas.read_excel(dest_filename, mailboxes.index(mailbox) + 1)
    for mailbox in mailboxes
]
allEmails = pandas.concat(emails)
pvtable = (
    pandas.pivot_table(
        allEmails,
        "Sender",
        index=["Date"],
        columns="Mailbox",
        margins=True,
        aggfunc=numpy.count_nonzero,
    )
    .fillna(0)
    .sort_values(by=["Date", "All"], ascending=True)
)
with pandas.ExcelWriter(dest_filename, engine="openpyxl", mode="a") as writer:
    writer.book = wb
    writer.sheets = dict((ws.title, ws) for ws in wb.worksheets)
    pvtable.to_excel(writer, "Summary")

widthColumnAdjust()
send(lastWeekDateTime, dest_filename)

end = time.time()
print(f"Finished in {end-start} seconds")
input()
