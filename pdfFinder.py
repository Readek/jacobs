from datetime import *
import shutil
import os
import time
from tika import parser
import json
import archive
import validators
import pyodbc
import pytz
import pyinputplus as pyip


class Finder:

    invoiceInput = pyip.inputStr("Enter invoice numbers separated by coma ',': ")
    supplierInput = pyip.inputStr("Enter supplier names separated by coma ',': ")

    def __init__(self):
        self.invoiceList = list(self.invoiceInput.split(", "))
        self.supplierList = list(self.supplierInput.split(", "))
        self.invSupList = list(zip(self.invoiceList, self.supplierList))
        self.saveDirectory = "C:\\Users\\RG053306\\Desktop\\Invoice Backups\\"

    def invoiceQuery(self, invSupList):
        easternTZ = pytz.timezone("US/Eastern")
        lasvegasTZ = pytz.timezone("US/Pacific")
        cnxn = pyodbc.connect(
            r"Driver={SQL Server};Server=OAKSQL21;Database=JIDS_Reporting;Trusted_Connection=yes;"
        )
        cursor = cnxn.cursor()
        query = "SELECT Last_task_Completed_Date, OU FROM [dbo].[vw_fact_liquid_office_invoice] WHERE Invoice_Number = ? AND Supplier_Name LIKE ? AND (Invoice_ID != '' OR Current_Location IS NULL)"

        rows = []
        for pair in self.invSupList:
            supplierName = f"%{pair[1]}%"
            try:
                cursor.execute(query, (pair[0], supplierName))
            except Exception:
                print(Exception)
                print("Further testing needed")
            row = cursor.fetchone()
            if row:
                rows.append(
                    [
                        (
                            easternTZ.localize(row.Last_task_Completed_Date),
                            lasvegasTZ.localize(row.Last_task_Completed_Date),
                        ),
                        row.OU,
                    ]
                )
            else:
                print(
                    f"Unable to find invoice {pair[0]}, please make sure you have entered eveything correctly, if you did, manual check is required."
                )

        cnxn.close()
        if rows:
            return rows
        return

    def FolderCheck(self, row, invoice):
        if row[1] == "":
            correctCountry = False
            while correctCountry == False:
                country = input(
                    f'Specify country for invoice {invoice}: for Canada - "CA", Chile - "CLP", Ireland - "IE", USA - "US" UK - "UK", Poland - "PL", India - "IN", Qatar - "QA", UAE - "AE", Italy - "IT", Deleted - "DEL": '
                )
                correctCountry = validators.countryValidity(country)
        elif row[1] == "CL_OU":
            country = "CLP"
        elif row[1] == "JV_OU_AED":
            country = "AE"
        elif row[1] == "JV_OU_AUD":
            country = "AU"
        elif row[1] == "JV_OU_CLP":
            country = "CLP"
        elif row[1] == "JV_OU_EUR":
            country = "US"
        elif row[1] == "JV_OU_GBP":
            country = "UK"
        elif row[1] == "JV_OU_NZD":
            country = "NZ"
        elif row[1] == "JV_OU_USD":
            country = "US"
        elif row[1] == "DE_OU":
            country = "DE"

        else:
            country = row[1][:2]

        def CountrySelect(country):
            if country.upper() == "CA":
                mainFolder = "CAInvoice"
            elif country.upper() == "IE":
                mainFolder = "IRInvoice"
            elif country.upper() == "US":
                mainFolder = "LOToTELE"
            elif country.upper() == "UK":
                mainFolder = "UKInvoice"
            elif country.upper() == "PL":
                mainFolder = "Poland"
            elif country.upper() == "IN":
                mainFolder = "India"
            elif country.upper() == "QA":
                mainFolder = "Qatar"
            elif country.upper() == "AE":
                mainFolder = "UAE"
            elif country.upper() == "CLP":
                mainFolder = "South America"
            elif country.upper() == "IT":
                mainFolder = "Italy"
            elif country.upper() == "AU":
                mainFolder = "Australia"
            elif country.upper() == "NZ":
                mainFolder = "New Zealand"
            elif country.upper() == "DE":
                mainFolder = "Germany"
            elif country.upper() == "DEL":
                mainFolder = "Delete"
            else:
                print("There is no such country")
            return mainFolder

        months = {
            1: "JAN",
            2: "FEB",
            3: "MAR",
            4: "APR",
            5: "MAY",
            6: "JUN",
            7: "JUL",
            8: "AUG",
            9: "SEP",
            10: "OCT",
            11: "NOV",
            12: "DEC",
        }
        months_full = {
            1: "January",
            2: "February",
            3: "March",
            4: "April",
            5: "May",
            6: "June",
            7: "July",
            8: "August",
            9: "September",
            10: "October",
            11: "November",
            12: "December",
        }
        directoryDateList = []
        for timezone in row[0]:
            directoryDate = str(timezone.month) + str(timezone.day) + str(timezone.year)
            if directoryDate not in directoryDateList:
                directoryDateList.append(directoryDate)
        filePathList = []
        for date in row[0]:
            for directoryDate in directoryDateList:
                directory = country.upper() + directoryDate
                if os.path.exists(f"H:\\{CountrySelect(country)}\\{directoryDate}\\"):
                    filePath = f"H:\\{CountrySelect(country)}\\{directoryDate}\\"
                    filePathList.append(filePath)
                    if os.path.exists(
                        f"H:\\{CountrySelect(country)}\\Archive\\{directory}\\"
                    ):
                        filePath = (
                            f"H:\\{CountrySelect(country)}\\Archive\\{directory}\\"
                        )
                        filePathList.append(filePath)
                elif os.path.exists(
                    f"H:\\{CountrySelect(country)}\\Archive\\{directory}\\"
                ):
                    filePath = f"H:\\{CountrySelect(country)}\\Archive\\{directory}\\"
                    filePathList.append(filePath)
                elif os.path.exists(
                    f"H:\\{CountrySelect(country)}\\Archive\\{months[date.month]}\\{directory}\\"
                ):
                    filePath = f"H:\\{CountrySelect(country)}\\Archive\\{months[date.month]}\\{directory}\\"
                    filePathList.append(filePath)
                elif os.path.exists(
                    f"H:\\{CountrySelect(country)}\\Archive\\{months_full[date.month]}\\{directory}\\"
                ):
                    filePath = f"H:\\{CountrySelect(country)}\\Archive\\{months_full[date.month]}\\{directory}\\"
                    filePathList.append(filePath)
                elif os.path.exists(
                    f"H:\\{CountrySelect(country)}\\Archive\\FY19\\{months[date.month]}\\{directory}\\"
                ):
                    filePath = f"H:\\{CountrySelect(country)}\\Archive\\FY19\\{months[date.month]}\\{directory}\\"
                    filePathList.append(filePath)
                elif os.path.exists(
                    f"H:\\{CountrySelect(country)}\\Archive\\FY19\\{months_full[date.month]}\\{directory}\\"
                ):
                    filePath = f"H:\\{CountrySelect(country)}\\Archive\\FY19\\{months_full[date.month]}\\{directory}\\"
                    filePathList.append(filePath)
                else:
                    print("Directory does not exist.")
        return filePathList

    def DirectoryCreation(self, invoice):
        invoiceSaveDirectory = os.path.join(self.saveDirectory, invoice)
        if not os.path.exists(invoiceSaveDirectory):
            os.mkdir(invoiceSaveDirectory)
        return invoiceSaveDirectory

    def SeekInDirectory(self, filePathList, invoice, row, invoiceSaveDirectory):
        for timezone in row[0]:
            polishTime = timezone.astimezone(pytz.timezone("Europe/Warsaw"))
            dateStartTimestamp = polishTime.timestamp() - 10
            dateEndTimestamp = polishTime.timestamp() + 10
            for filePath in filePathList:
                fileList = []
                for fileEntry in os.scandir(filePath):
                    if (
                        fileEntry.is_file()
                        and ".bhf" not in fileEntry.name
                        and dateStartTimestamp
                        <= fileEntry.stat().st_mtime
                        <= dateEndTimestamp
                    ):
                        print(fileEntry.name)
                        shutil.copy(fileEntry.path, invoiceSaveDirectory)

    def SeekInPDF(self, invoice, invoiceSaveDirectory):
        os.chdir(invoiceSaveDirectory)
        for file in os.listdir(invoiceSaveDirectory):
            if file.endswith("1.pdf"):
                raw = json.dumps(parser.from_file(file))
                if invoice in raw:
                    print(f"Found invoice number {invoice} in file {file}")
                    break

    def main(self):
        archive.archiveFolders(self.saveDirectory)
        if self.invoiceQuery(self.invSupList):
            for counter, row in enumerate(self.invoiceQuery(self.invSupList)):
                print(row)
                self.SeekInDirectory(
                    self.FolderCheck(row, self.invoiceList[counter]),
                    self.invoiceList[counter],
                    row,
                    self.DirectoryCreation(self.invoiceList[counter]),
                )
                print("Now looking for invoice number in PDFs.")
                if self.SeekInPDF(
                    self.invoiceList[counter],
                    self.DirectoryCreation(self.invoiceList[counter]),
                ):
                    break
                else:
                    print("This invoice number was not found")
                    for filePath in self.FolderCheck(row, self.invoiceList[counter]):
                        self.SeekInPDF(self.invoiceList[counter], filePath)

            input()


if __name__ == "__main__":
    objName = Finder()
    objName.main()
