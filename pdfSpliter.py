import tkinter as tk
from tkinter import filedialog
import PyPDF2
import os
import re
import win32com.client as win32

# testy znakow, tego czy ktos nie wpisze wiekszej strony niz max w pliku, czy poczatek zakresu nie jest wieszy niz jego koniec
class Split:
    countries = [
        ("United States", "US"),
        ("Canada", "CA"),
        ("United Kingdom", "UK"),
        ("Ireland", "IE"),
        ("Poland", "PL"),
        ("Chile", "CL"),
        ("India", "IN"),
        ("Qatar", "QA"),
        ("United Arab Emirates", "AE"),
    ]
    root = tk.Tk()
    button_frame = tk.Frame(root)
    root.title("PDF Splitter")
    countrySelect = tk.StringVar()
    countrySelect.set(countries[0][1])
    pages = tk.Label(root, text="Pages:")
    backups = tk.Label(root, text="Backups:")
    fileEntryDesc = tk.Label(root, text="Splitting file location:")
    directoryEntryDecs = tk.Label(root, text="Save location:")
    e_pages = tk.Entry(root, width=45)
    e_backups = tk.Entry(root, width=45)
    fileOpenButton = ""
    directoryOpenButton = ""
    e_filepath = tk.Entry(root, width=56)
    e_filepath.insert(0, "")
    e_directory = tk.Entry(root, width=56)
    sendButton = ""
    splitButton = ""

    originalFile = ""
    scope = ""
    lastPages = ""

    def __init__(self):
        for counter, (country, abbr) in enumerate(self.countries):
            tk.Radiobutton(
                self.root, text=country, variable=self.countrySelect, value=abbr
            ).grid(row=counter, column=3, sticky="w")
            tk.Radiobutton.deselect
        self.fileOpenButton = tk.Button(
            self.root, text="Open file", width=7, command=self.__openFile
        )
        self.directoryOpenButton = tk.Button(
            self.root, text="Open folder", width=9, command=self.__selectSaveDirectory
        )
        self.sendButton = tk.Button(
            self.button_frame,
            text="Split and Send",
            width=12,
            command=lambda: self._newsendingwithfile(self._newsplitting()),
        )
        self.splitButton = tk.Button(
            self.button_frame, text="Split", width=12, command=self._newsplitting
        )
        self.__initGrid()
        self.root.mainloop()

    def __initGrid(self):
        self.pages.grid(row=0, column=0, sticky="w")
        self.backups.grid(row=1, column=0, sticky="w")
        self.fileEntryDesc.grid(row=2, column=0, columnspan=2, sticky="w")
        self.directoryEntryDecs.grid(row=5, column=0, sticky="w")
        self.e_pages.grid(row=0, column=1)
        self.e_backups.grid(row=1, column=1)
        self.e_filepath.grid(row=3, column=0, columnspan=3)
        self.fileOpenButton.grid(row=4, column=0, columnspan=3)
        self.e_directory.grid(row=6, column=0, columnspan=3)
        self.directoryOpenButton.grid(row=7, column=0, columnspan=2)
        self.button_frame.grid(row=len(self.countries) + 2, columnspan=4)
        self.splitButton.grid(row=0, column=0)
        self.sendButton.grid(row=0, column=1)

    def __clearDirectoryField(self):
        self.e_directory.delete(0, "end")

    def __clearFilepathField(self):
        self.e_filepath.delete(0, "end")

    def __addEntry(self):
        self.e_customEmail = tk.Entry(root, width=30, command=self.__addEntry)
        self.e_customEmail.grid(row=len(self.countries) + 1, column=0, columnspan=2)

    def __openFile(self):
        filename = tk.filedialog.askopenfilename(
            title="Select a file", filetypes=[("PDF", "*.pdf")]
        )
        self.__clearFilepathField()
        self.e_filepath.insert(0, filename)
        self.__clearDirectoryField()
        self.e_directory.insert(0, os.path.dirname(filename))

    def __selectSaveDirectory(self):
        directory = tk.filedialog.askdirectory(title="Please select a directory")
        self.__clearDirectoryField()
        self.e_directory.insert(0, directory)

    def _newsplitting(self):
        inputFile = open(self.e_filepath.get(), "rb")
        self.originalFile = PyPDF2.PdfFileReader(inputFile)
        self.scope = [x for x in self.e_pages.get().split(",")]
        self.lastPages = [x for x in self.e_backups.get().split(",")]
        os.chdir(self.e_directory.get())
        newInvoices = []
        for pages in self.scope:
            writer = PyPDF2.PdfFileWriter()
            if len(pages.split("-")) == 1:
                rangeFrom = rangeTo = int(pages)
            else:
                rangeFrom, rangeTo = map(int, pages.split("-"))
            for pageNum in range(rangeFrom - 1, rangeTo):
                writer.addPage(self.originalFile.getPage(pageNum))
            for backup in self.lastPages:
                if backup == "":
                    continue
                elif len(backup.split("-")) == 1:
                    rangeFrom = rangeTo = int(backup)
                else:
                    rangeFrom, rangeTo = map(int, backup.split("-"))
                for pageNum in range(rangeFrom - 1, rangeTo):
                    writer.addPage(self.originalFile.getPage(pageNum))
            strippedFileName = re.sub(
                r"[^a-zA-Z0-9]", "", os.path.split(self.e_filepath.get())[1][:-4]
            )
            newFileName = strippedFileName + str(self.scope.index(pages)) + ".pdf"
            output = open(newFileName, "wb")
            writer.write(output)
            output.close()
            newInvoices.append(newFileName)
            print(newFileName)
        inputFile.close()
        self.__clearFilepathField()
        return newInvoices

    def _newsendingwithfile(self, newInvoices):
        for invoice in newInvoices:
            mailAddress = f"{self.countrySelect.get()}APInvoices@jacobs.com"
            # mailAddress = "Radoslaw.Gasior@jacobs.com"
            outlook = win32.Dispatch("Outlook.Application")
            mail = outlook.CreateItem(0)
            mail.To = mailAddress
            mail.Subject = f"Invoice {newInvoices.index(invoice)} from {os.path.split(self.e_filepath.get())[1][:-4]}"
            mail.Body = ""
            attachment = os.path.join(self.e_directory.get(), invoice)
            mail.Attachments.Add(attachment)
            mail.Send()
        self.__clearFilepathField()


Split()

