import pyodbc
import pandas as pd
from pandas.tseries.offsets import BDay
import numpy as np
import datetime as dt
import openpyxl
import win32com.client as win32
import os
import datetime as dt
import pyinputplus as pyip


RESULTS_FOLDER = "C:\\Users\\RG053306\\Documents\\Reports\\Performance report\\Results"
EXCEL = "C:\\Users\\RG053306\\Documents\\Reports\\Performance report\\Data.xlsx"
STORAGE = "C:\\Users\\RG053306\\Documents\\Reports\\Performance report\\Bulk data\\Save dfs"
steps = ["Start", "Review"]
skip_list = ["Archive"]


def number_of_days():

    end_date = dt.datetime.today().date()
    start_date = pyip.inputDate(
        "Please enter date when reports were last ran, in format DD-MM-YYYY: ",
        formats=["%d-%m-%Y"],
    )
    delta = (end_date - start_date).days
    if delta < 0:
        print("Reports could not be ran in the future, check the date.")
        return number_of_days()
    if delta > 14:
        response = pyip.inputYesNo(
            f"You have entered a date that was {delta} days ago. Are you sure you have entered correct date? (yes/no) "
        )
        if response == "yes":
            if delta > 30:
                print(
                    "Now you want records from the month ago? Sorry, but no, ask IT to do that for you."
                )
                return number_of_days()
            return delta
        elif response == "no":
            return number_of_days()
    return delta


def check_server_date():
    cnxn = pyodbc.connect(
        r"Driver={SQL Server};Server=OAKSQL20;Database=LOHist;Trusted_Connection=yes;"
    )
    cursor = cnxn.cursor()
    cursor.execute("SELECT CURRENT_TIMESTAMP as DATE")
    server_date = cursor.fetchone()
    if server_date[0] < dt.datetime.today():
            primary_hours = [-9, 15]
    else:
        primary_hours = [-33, -9]
    return primary_hours


def database_import(step, day, primary_hours):
    cnxn = pyodbc.connect(
        r"Driver={SQL Server};Server=OAKSQL20;Database=LOHist;Trusted_Connection=yes;"
    )
    cursor = cnxn.cursor()
    offset = day * 24

    start_query = f""""""

    review_query = f""""""

    if step == steps[0]:
        cursor.execute(start_query)
        rows = cursor.fetchall()
    elif step == steps[1]:
        cursor.execute(review_query)
        rows = cursor.fetchall()
    return rows


def rows_to_dataframe(rows):
    df = pd.DataFrame.from_records(
        rows, columns=["EVENTRECORDID", "SENDERFULLNAME", "RETURNTOWQNAME"]
    )

    df["Team"] = "Other"
    return df


def dataframe_to_pivot(dataframe, people):
    today = dt.datetime.today()
    for team in people:
        for key in team.keys():
            dataframe.loc[dataframe["SENDERFULLNAME"].isin(team[key]), "Team"] = key

    pivot = (
        dataframe.groupby(["Team", "SENDERFULLNAME", "RETURNTOWQNAME"])[
            "RETURNTOWQNAME"
        ]
        .count()
        .sort_index()
    )
    print(pivot)
    return pivot


def list_create():
    queues = []
    people = []
    wb = openpyxl.load_workbook(EXCEL)
    ws1 = wb["Processing queues"]
    ws2 = wb["Employees"]
    keys = []
    items = []
    for col in ws1.iter_cols():
        column = []
        keys.append(col[0].value)
        for cell in col[1:]:
            if cell.value != None:
                column.append(cell.value)
        items.append(column)
        for i, key in enumerate(keys):
            d = {key: items[i]}
        queues.append(d)
    for col in ws2.iter_cols():
        column = []
        keys.append(col[0].value)
        for cell in col[1:]:
            if cell.value != None:
                column.append(cell.value)
        items.append(column)
        for i, key in enumerate(keys):
            d = {key: items[i]}
        people.append(d)
    return queues, people


def pivot_to_excel(pivot):

    wb = openpyxl.Workbook()
    wb.save(filename=os.path.join(RESULTS_FOLDER, f"0.xlsx"))

    with pd.ExcelWriter(
        os.path.join(RESULTS_FOLDER, f"0.xlsx"), engine="openpyxl", mode="a"
    ) as writer:
        writer.book = wb
        pivot.to_excel(writer)
        wb.remove(wb["Sheet"])


def beautify(day, primary_hours):
    start_list = ["Team", "Employee name", "Work queue", "Invoices started"]
    review_list = ["Team", "Employee name", "Work queue", "Invoices reviewed"]
    today = dt.datetime.today()
    report_date_dt = today - dt.timedelta(days=(1 + day))
    report_date = (report_date_dt).strftime("%d%b%y").upper()
    for file in os.listdir(RESULTS_FOLDER):
        if file in skip_list:
            pass
        else:
            wb = openpyxl.load_workbook(os.path.join(RESULTS_FOLDER, f"{file}"))
            ws = wb["Sheet1"]
            if "LO_GBS" in ws["C2"].value:
                name = "Start"
                for column in ws.iter_cols(max_row=1):
                    for cell in column:
                        cell.value = start_list[cell.column - 1]
                        ws.column_dimensions[cell.column_letter].width = 25
            elif "AP_GBS" in ws["C2"].value:
                name = "Review"
                for column in ws.iter_cols(max_row=1):
                    for cell in column:
                        cell.value = review_list[cell.column - 1]
                        ws.column_dimensions[cell.column_letter].width = 25
            wb.save(filename=os.path.join(RESULTS_FOLDER, f"{file}"))

            os.rename(
                os.path.join(RESULTS_FOLDER, f"{file}"),
                os.path.join(RESULTS_FOLDER, f"{name}_report_{report_date}.xlsx"),
            )
    return report_date_dt

def save_df(step, df, report_date_dt):
    df.to_pickle(os.path.join(STORAGE, f"{step}_DF-{report_date_dt.day}-{report_date_dt.month}-{report_date_dt.year}.pkl"))

def cleanup():
    for file in os.listdir(RESULTS_FOLDER):
        if file == "Archive":
            continue
        else:
            if file in os.listdir(os.path.join(RESULTS_FOLDER, "Archive")):
                os.remove(os.path.join(RESULTS_FOLDER, file))
            else:
                os.rename(
                    os.path.join(RESULTS_FOLDER, file),
                    os.path.join(RESULTS_FOLDER, "Archive", file),
                )


def send(report_date_dt):
    outlook = win32.Dispatch("outlook.application")
    mail = outlook.CreateItem(0)
    # mailAddress = ""
    mailAddress = ""
    mail.To = mailAddress
    mail.CC = ""
    mail.Subject = f"Start and Review reports"
    for file in os.listdir(RESULTS_FOLDER):
        if file in skip_list:
            continue
        else:
            mail.Attachments.Add(os.path.join(RESULTS_FOLDER, file))
            skip_list.append(file)
    mail.Body = f"""Hello,

Please find start and review reports for {report_date_dt.day}-{report_date_dt.month}-{report_date_dt.year}.

Thanks,
Radek"""
    mail.Send()


cleanup()
days = number_of_days()
primary_hours = check_server_date()
queues, people = list_create()
report_date_dt = ""
report_date_list = []
for day in range(days):
    for step in steps:
        print(step)
        rows = database_import(step, day, primary_hours)
        if len(rows) < 20:
            print(f"Tyle rekordow: {len(rows)}")
            continue
        else:
            df = rows_to_dataframe(rows)
            pivot_to_excel(dataframe_to_pivot(df, people))
            report_date_dt = beautify(day, primary_hours)
            save_df(step, df, report_date_dt)
    if report_date_dt:
        send(report_date_dt)
