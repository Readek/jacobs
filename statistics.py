from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import openpyxl
from openpyxl.utils import get_column_letter
from openpyxl.styles import Border, Side, NamedStyle
import datetime as dt
import os
import shutil
from webdriver_manager.chrome import ChromeDriverManager


def archive():
    today = dt.datetime.today()
    directory = ()
    if not os.path.exists("Archive"):
        os.mkdir("Archive")
    if not os._exists(
        os.path.join(
            os.getcwd(),
            "Archive",
            f"Statystyki_{today.day}-{today.month}-{today.year}.xlsx",
        )
    ):
        shutil.copy(
            os.path.join(directory, "Statystyki.xlsx"),
            os.path.join(
                os.getcwd(),
                "Archive",
                f"Statystyki_{today.day}-{today.month}-{today.year}.xlsx",
            ),
        )


def retrieveQnames():
    queues_list = []
    wb = openpyxl.load_workbook(EXCEL)
    ws1 = wb["all queues"]
    for column in ws1.iter_cols(min_col=2, max_col=30, min_row=2, max_row=2):
        for cell in column:
            if cell.value is None:
                pass
            else:
                queues_list.append(cell.value)
    wb.close()
    return queues_list


def check(queues_list):
    today = dt.datetime.today()
    twoweeksago = today - dt.timedelta(days=14)
    volumes = []
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    driver.find_element_by_xpath('//*[@id="DFS__UserID"]').send_keys("lotest_user1")
    driver.find_element_by_xpath(
        '//*[@id="tagFormLogin"]/table/tbody/tr/td/table/tbody/tr/td/fieldset/table/tbody/tr[2]/td[2]/input'
    ).send_keys("Welcome1")
    driver.find_element_by_xpath(
        '//*[@id="tagFormLogin"]/table/tbody/tr/td/table/tbody/tr/td/fieldset/table/tbody/tr[3]/td[2]/input[1]'
    ).click()
    driver.find_element_by_xpath(
        '//*[@id="bottom_panel_row"]/td[1]/table/tbody/tr/td[9]/a'
    ).click()
    driver.find_element_by_xpath('//*[@id="btnShowMyQueues"]').click()

    for queue in queues_list:
        child = driver.find_element_by_link_text(queue)
        parent = child.find_element_by_xpath("../..").text
        volumes.append(str(parent.split()[-1]))
        dictionary = dict(zip(queues_list, volumes))
    driver.find_element_by_xpath('//*[@id="1381"]/td[5]/a').click()
    volumesbydayAP = {}
    for i in range((today - twoweeksago).days + 1):
        day = twoweeksago + dt.timedelta(days=i)
        date = day.strftime(f"%b {day.day}, %Y")
        volumesbydayAP[date] = 0

    if int(dictionary[""]) < 500:
        for key in volumesbydayAP:
            testlist = driver.find_elements_by_xpath(f"//*[contains(text(), '{key}')]")
            volumesbydayAP[key] += len(testlist)
    else:
        pages = (int(dictionary[""]) // 500) + 1
        for i in range(pages):
            for key in volumesbydayAP:
                testlist = driver.find_elements_by_xpath(
                    f"//*[contains(text(), '{key}')]"
                )
                volumesbydayAP[key] += len(testlist)
            driver.find_element_by_xpath(
                '//*[@id="WorkQueue_ButtonsPanel"]/tbody/tr/td[3]/table/tbody/tr/td/table/tbody/tr/td[4]/input'
            ).click()

    driver.find_element_by_xpath(
        '//*[@id="bottom_panel_row"]/td[1]/table/tbody/tr/td[9]/a'
    ).click()
    driver.find_element_by_xpath('//*[@id="btnShowMyQueues"]').click()
    driver.find_element_by_xpath('//*[@id="1380"]/td[5]/a').click()

    volumesbydayLO = {}
    for i in range((today - twoweeksago).days + 1):
        day = twoweeksago + dt.timedelta(days=i)
        date = day.strftime(f"%b {day.day}, %Y")
        volumesbydayLO[date] = 0
    if int(dictionary[""]) < 500:
        for key in volumesbydayLO:
            testlist = driver.find_elements_by_xpath(f"//*[contains(text(), '{key}')]")
            volumesbydayLO[key] += len(testlist)
    else:
        pages = (int(dictionary[""]) // 500) + 1
        for i in range(pages):
            for key in volumesbydayLO:
                testlist = driver.find_elements_by_xpath(
                    f"//*[contains(text(), '{key}')]"
                )
                volumesbydayLO[key] += len(testlist)
            driver.find_element_by_xpath(
                '//*[@id="WorkQueue_ButtonsPanel"]/tbody/tr/td[3]/table/tbody/tr/td/table/tbody/tr/td[4]/input'
            ).click()

    return [dictionary, volumesbydayLO, volumesbydayAP]


def update(args):
    today = dt.datetime.today()
    todaystrf = today.strftime("%Y-%m-%d 00:00:00")

    wb = openpyxl.load_workbook(EXCEL)
    ws1 = wb["all queues"]

    for row in ws1.iter_rows(min_col=1, max_col=1):
        for cell in row:
            if str(cell.value) == todaystrf:
                today_row = cell.row
    for column in ws1.iter_cols(min_col=2, max_col=30, min_row=2, max_row=2):
        for key in args[0]:
            for cell in column:
                if str(cell.value) == key:
                    ws1[f"{cell.column_letter}{today_row}"] = int(args[0][key])

    volumesbydayLO = {
        key: args[1][key] for key in args[1] if (args[1][key] != 0 or args[2][key] != 0)
    }
    volumesbydayAP = {
        key: args[2][key] for key in args[2] if (args[2][key] != 0 or args[1][key] != 0)
    }

    ws2 = wb["US dates"]
    if len(volumesbydayAP) > len(volumesbydayLO):
        bigger = volumesbydayAP
    else:
        bigger = volumesbydayLO
    datelist = []
    ws2[f"A{ws2.max_row+1}"].style = "Accent3"
    ws2.merge_cells(f"A{ws2.max_row}:D{ws2.max_row}")
    active_cell = ws2[f"A{ws2.max_row+1}"]
    active_cell.number_format = "[$-F800]dddd\,\ mmmm\ dd\,\ yyyy"
    active_cell.value = today

    for key in bigger:
        datelist.append(dt.datetime.strptime(key, "%b %d, %Y"))

    counter = 0
    for row in range(ws2.max_row, ws2.max_row + len(bigger)):
        ws2[f"B{row}"].value = datelist[counter]
        ws2[f"B{row}"].number_format = "mm-dd-yy"
        try:
            ws2[f"C{row}"].value = volumesbydayLO[list(volumesbydayLO.keys())[counter]]
        except:
            ws2[f"C{row}"].value = 0
        try:
            ws2[f"D{row}"].value = volumesbydayAP[list(volumesbydayAP.keys())[counter]]
        except:
            ws2[f"D{row}"].value = 0
        counter += 1

    for column in ws2.iter_cols(
        min_col=1, max_col=4, min_row=ws2.max_row - 14, max_row=ws2.max_row
    ):
        for cell in column:
            cell.border = Border(
                left=Side(style="thin"),
                right=Side(style="thin"),
                top=Side(style="thin"),
                bottom=Side(style="thin"),
            )

    wb.save(EXCEL)


archive()
update(check(retrieveQnames()))
