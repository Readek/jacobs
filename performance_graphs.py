import pandas as pd
import os
import datetime as dt
import plotly.express as px

def retrieve_data(step):

    path = f"C:\\Users\\RG053306\\Documents\\Reports\\Performance report\\Bulk data\\Finance {step} Report"
    li = []
    col_names = [
        "ID",
        "EVENTTYPE",
        "EVENTRECORDID",
        "TASKID",
        "ACTION",
        "SENDTOFULLNAME",
        "SENDTOTYPE",
        "RECFROMFULLNAME",
        "ORIGINATORFULLNAME",
        "SENDERFULLNAME",
        "DATAMODIFIEDBY",
        "RETURNTOWQID",
        "RETURNTOWQNAME",
        "ACTIONTIME",
        "SUBJECT",
        "GENERALINFO1",
    ]
    for filename in os.listdir(path):

        df = pd.read_csv(
            os.path.join(path, filename),
            error_bad_lines=False,
            names=col_names,
            encoding="utf-16",
            delimiter=";",
            skiprows=[1],
            skipfooter=1,
            index_col=None,
        )
        li.append(df)

    frame = pd.concat(li, axis=0, ignore_index=True)
    return frame

def team_assignment(frame):
    frame["Team"] = "Other"
    US_Team = [
        "Team members"
    ]

    EUMENA_Team = [
        "Team members"
]



    frame.loc[frame["SENDERFULLNAME"].isin(US_Team), "Team"] = "US Team"
    frame.loc[frame["SENDERFULLNAME"].isin(EUMENA_Team), "Team"] = "EUMENA Team"
    return frame

def names_correction(frame):
    frame.loc[frame["SENDERFULLNAME"] == "Jane Doe", "SENDERFULLNAME"] = "Jane Doe"

    return frame

def cleanup(frame, step):   
    frame = frame[frame["ID"] != ("Changed database context to 'LOHist'.")]
    frame = frame[frame["ID"] != ("--")]
    frame = frame[frame["ID"] != ("ID")]
    frame.drop_duplicates("ID", inplace=True)
    if frame["ACTIONTIME"].dtype == "object":
        frame["ACTIONTIME"] = frame["ACTIONTIME"].astype('datetime64[s]')
        frame["ACTIONTIME"] = frame["ACTIONTIME"].dt.tz_localize("US/Pacific").dt.tz_convert("Europe/Warsaw")
    frame.name = step
    frame.to_pickle(os.path.join("C:\\Users\\RG053306\\Documents\\Reports\\Performance report\\Bulk data\\", f"{step}_frame.pkl"))

def load_new_data(step):
    frame = pd.read_pickle(os.path.join("C:\\Users\\RG053306\\Documents\\Reports\\Performance report\\Bulk data\\", f"{step}_frame.pkl"))
    for file in os.listdir(os.path.join("C:\\Users\\RG053306\\Documents\\Reports\\Performance report\\Bulk data\\", "Save dfs")):
        if file.startswith(step):
            df = pd.read_pickle(os.path.join("C:\\Users\\RG053306\\Documents\\Reports\\Performance report\\Bulk data\\", "Save dfs", file))
            df["ACTIONTIME"] = df["ACTIONTIME"].astype('datetime64[s]')        
            df["ACTIONTIME"] = df["ACTIONTIME"].dt.tz_localize("US/Pacific").dt.tz_convert("Europe/Warsaw")
            frame = frame.append(df, ignore_index=True)
    return frame


def get_frames():

    frame_start = pd.read_pickle(os.path.join("C:\\Users\\RG053306\\Documents\\Reports\\Performance report\\Bulk data\\", "Start_frame.pkl"))
    frame_start.name = "Start"
    frame_review = pd.read_pickle(os.path.join("C:\\Users\\RG053306\\Documents\\Reports\\Performance report\\Bulk data\\", "Review_frame.pkl"))
    frame_review.name = "Review"
    frames = [frame_start, frame_review]
    return frames

def transformation_and_graphs(frames):

    frame_all = pd.concat(frames, axis=0, ignore_index=True)

    frame_US_all = frame_all.loc[(frame_all["Team"] == "US Team")]
    frame_EU_all = frame_all.loc[(frame_all["Team"] == "EUMENA Team")]

    for frame in frames:
        if frame.name == "Start":
            keyword = "start"
        else:
            keyword = "review"

        #Team focused dataframes

        frame_US = frame.loc[(frame["Team"] == "US Team")]
        frame_EU = frame.loc[(frame["Team"] == "EUMENA Team")]

        #Data transofrmation

        pivot_team = frame.groupby(["Team", "SENDERFULLNAME",frame["ACTIONTIME"]])["ACTIONTIME"].count().reset_index(name="INVOICES")
        pivot_team_all = frame_all.groupby(["Team", "SENDERFULLNAME",frame_all["ACTIONTIME"]])["ACTIONTIME"].count().reset_index(name="INVOICES")


        invoices_per_month_by_person_US = frame_US.groupby(["Team",frame_US["ACTIONTIME"].dt.to_period("M"), "SENDERFULLNAME",])["Team"].count().reset_index(name="INVOICES")
        invoices_per_month_by_person_US["ACTIONTIME"] = invoices_per_month_by_person_US["ACTIONTIME"].dt.strftime("%b")

        invoices_per_month_by_person_US_all = frame_US_all.groupby(["Team",frame_US_all["ACTIONTIME"].dt.to_period("M"), "SENDERFULLNAME",])["Team"].count().reset_index(name="INVOICES")
        invoices_per_month_by_person_US_all["ACTIONTIME"] = invoices_per_month_by_person_US_all["ACTIONTIME"].dt.strftime("%b")

        invoices_per_month_by_person_EU = frame_EU.groupby(["Team",frame_EU["ACTIONTIME"].dt.to_period("M"), "SENDERFULLNAME",])["Team"].count().reset_index(name="INVOICES")
        invoices_per_month_by_person_EU["ACTIONTIME"] = invoices_per_month_by_person_EU["ACTIONTIME"].dt.strftime("%b")

        invoices_per_month_by_person_EU_all = frame_EU_all.groupby(["Team",frame_EU_all["ACTIONTIME"].dt.to_period("M"), "SENDERFULLNAME",])["Team"].count().reset_index(name="INVOICES")
        invoices_per_month_by_person_EU_all["ACTIONTIME"] = invoices_per_month_by_person_EU_all["ACTIONTIME"].dt.strftime("%b")

        invoices_per_day_by_person_US =  frame_US.groupby(["Team", "SENDERFULLNAME",frame_US["ACTIONTIME"].dt.date])["ACTIONTIME"].count().reset_index(name="INVOICES")
        invoices_per_day_by_person_EU =  frame_EU.groupby(["Team", "SENDERFULLNAME",frame_EU["ACTIONTIME"].dt.date])["ACTIONTIME"].count().reset_index(name="INVOICES")

        invoices_per_day_by_person_US_all =  frame_US_all.groupby(["Team", "SENDERFULLNAME",frame_US_all["ACTIONTIME"].dt.date])["ACTIONTIME"].count().reset_index(name="INVOICES")
        invoices_per_day_by_person_EU_all =  frame_EU_all.groupby(["Team", "SENDERFULLNAME",frame_EU_all["ACTIONTIME"].dt.date])["ACTIONTIME"].count().reset_index(name="INVOICES")

        total_per_month = pivot_team.groupby(["Team",pivot_team["ACTIONTIME"].dt.to_period("M")]).sum().reset_index()
        total_per_month["ACTIONTIME"] = total_per_month["ACTIONTIME"].dt.strftime("%b")

        total_per_month_all = pivot_team_all.groupby(["Team",pivot_team_all["ACTIONTIME"].dt.to_period("M")]).sum().reset_index()
        total_per_month_all["ACTIONTIME"] = total_per_month_all["ACTIONTIME"].dt.strftime("%b")

        invoices_per_month_by_queue = frame.groupby(["RETURNTOWQNAME",frame["ACTIONTIME"].dt.to_period("M")])["Team"].count().reset_index(name="INVOICES")
        invoices_per_month_by_queue["ACTIONTIME"] = invoices_per_month_by_queue["ACTIONTIME"].dt.strftime("%b")

        invoices_per_month_by_queue_all = frame_all.groupby(["RETURNTOWQNAME",frame_all["ACTIONTIME"].dt.to_period("M")])["Team"].count().reset_index(name="INVOICES")
        invoices_per_month_by_queue_all["ACTIONTIME"] = invoices_per_month_by_queue_all["ACTIONTIME"].dt.strftime("%b")
        
        #Graphs
        invoices_per_day_by_person_US_graph = px.line(invoices_per_day_by_person_US, x="ACTIONTIME", y="INVOICES", color="SENDERFULLNAME", text="INVOICES", labels={"ACTIONTIME":"Date", "INVOICES":"Invoices"}, title=f"Invoices {keyword}ed by person per date")
        invoices_per_day_by_person_EU_graph = px.line(invoices_per_day_by_person_EU, x="ACTIONTIME", y="INVOICES", color="SENDERFULLNAME", text="INVOICES", labels={"ACTIONTIME":"Date", "INVOICES":"Invoices"}, title=f"Invoices {keyword}ed by person per date")
        invoices_per_day_by_person_US_all_graph = px.line(invoices_per_day_by_person_US_all, x="ACTIONTIME", y="INVOICES", color="SENDERFULLNAME", text="INVOICES", labels={"ACTIONTIME":"Date", "INVOICES":"Invoices"}, title=f"Invoices processed by person per date")
        invoices_per_day_by_person_EU_all_graph = px.line(invoices_per_day_by_person_EU_all, x="ACTIONTIME", y="INVOICES", color="SENDERFULLNAME", text="INVOICES", labels={"ACTIONTIME":"Date", "INVOICES":"Invoices"}, title=f"Invoices processed by person per date")
        
        invoices_per_month_by_person_US_graph = px.bar(invoices_per_month_by_person_US, x="SENDERFULLNAME", y="INVOICES", color="ACTIONTIME", text="INVOICES", labels={"SENDERFULLNAME":"Employee", "INVOICES":"Invoices"}, title=f"Invoices {keyword}ed by person per month", hover_name="SENDERFULLNAME")
        invoices_per_month_by_person_EU_graph = px.bar(invoices_per_month_by_person_EU, x="SENDERFULLNAME", y="INVOICES", color="ACTIONTIME", text="INVOICES", labels={"SENDERFULLNAME":"Employee", "INVOICES":"Invoices"}, title=f"Invoices {keyword}ed by person per month", hover_name="SENDERFULLNAME")
        invoices_per_month_by_person_US_all_graph = px.bar(invoices_per_month_by_person_US_all, x="SENDERFULLNAME", y="INVOICES", color="ACTIONTIME", text="INVOICES", labels={"SENDERFULLNAME":"Employee", "INVOICES":"Invoices"}, title=f"Invoices processed by person per month", hover_name="SENDERFULLNAME")
        invoices_per_month_by_person_EU_all_graph = px.bar(invoices_per_month_by_person_EU_all, x="SENDERFULLNAME", y="INVOICES", color="ACTIONTIME", text="INVOICES", labels={"SENDERFULLNAME":"Employee", "INVOICES":"Invoices"}, title=f"Invoices processed by person per month", hover_name="SENDERFULLNAME")
        
        invoices_per_person_by_month_US_graph = px.bar(invoices_per_month_by_person_US, x="ACTIONTIME", y="INVOICES", color="SENDERFULLNAME",text="INVOICES", labels={"ACTIONTIME":"Month", "INVOICES":"Invoices"}, title=f"Invoices {keyword}ed by person each month")
        invoices_per_person_by_month_EU_Graph = px.bar(invoices_per_month_by_person_EU, x="ACTIONTIME", y="INVOICES", color="SENDERFULLNAME",text="INVOICES", labels={"ACTIONTIME":"Month", "INVOICES":"Invoices"}, title=f"Invoices {keyword}ed by person each month")
        invoices_per_person_by_month_US_all_graph = px.bar(invoices_per_month_by_person_US_all, x="ACTIONTIME", y="INVOICES", color="SENDERFULLNAME",text="INVOICES", labels={"ACTIONTIME":"Month", "INVOICES":"Invoices"}, title=f"Invoices processed by person each month")
        invoices_per_person_by_month_EU_all_graph = px.bar(invoices_per_month_by_person_EU_all, x="ACTIONTIME", y="INVOICES", color="SENDERFULLNAME",text="INVOICES", labels={"ACTIONTIME":"Month", "INVOICES":"Invoices"}, title=f"Invoices processed by person each month")

        invoices_per_team_by_month_graph = px.bar(total_per_month, x=total_per_month["ACTIONTIME"], y="INVOICES", color="Team",text="INVOICES", labels={"ACTIONTIME":"Month", "INVOICES":"Invoices"}, title=f"Invoices {keyword}ed by team per month")
        invoices_per_team_by_month_all_graph = px.bar(total_per_month_all, x=total_per_month_all["ACTIONTIME"], y="INVOICES", color="Team",text="INVOICES", labels={"ACTIONTIME":"Month", "INVOICES":"Invoices"}, title=f"Invoices processed by team per month")
        
        invoices_per_month_by_queue_graph = px.bar(invoices_per_month_by_queue, x="ACTIONTIME", y="INVOICES", color="RETURNTOWQNAME",text="INVOICES", labels={"ACTIONTIME":"Month", "INVOICES":"Invoices"}, title=f"Invoices {keyword}ed from queue by month")


        with open(f"AP graphs_{keyword}-US.html", "a") as f:
            f.write(invoices_per_day_by_person_US_graph.to_html(full_html=False, include_plotlyjs="cdn"))
            f.write(invoices_per_month_by_person_US_graph.to_html(full_html=False, include_plotlyjs="cdn"))
            f.write(invoices_per_person_by_month_US_graph.to_html(full_html=False, include_plotlyjs="cdn"))
            f.write(invoices_per_team_by_month_graph.to_html(full_html=False, include_plotlyjs="cdn"))
            f.write(invoices_per_month_by_queue_graph.to_html(full_html=False, include_plotlyjs="cdn"))


        with open(f"AP graphs_{keyword}-EU.html", "a") as f:
            f.write(invoices_per_day_by_person_EU_graph.to_html(full_html=False, include_plotlyjs="cdn"))
            f.write(invoices_per_month_by_person_EU_graph.to_html(full_html=False, include_plotlyjs="cdn"))
            f.write(invoices_per_person_by_month_EU_Graph.to_html(full_html=False, include_plotlyjs="cdn"))
            f.write(invoices_per_team_by_month_graph.to_html(full_html=False, include_plotlyjs="cdn"))
            f.write(invoices_per_month_by_queue_graph.to_html(full_html=False, include_plotlyjs="cdn"))

    with open(f"AP graphs_both_steps-US.html", "a") as f:
        f.write(invoices_per_day_by_person_US_all_graph.to_html(full_html=False, include_plotlyjs="cdn"))
        f.write("<br></br>")
        f.write(invoices_per_month_by_person_US_all_graph.to_html(full_html=False, include_plotlyjs="cdn"))
        f.write("<br></br>")
        f.write(invoices_per_person_by_month_US_all_graph.to_html(full_html=False, include_plotlyjs="cdn"))
        f.write("<br></br>")
        f.write(invoices_per_team_by_month_all_graph.to_html(full_html=False, include_plotlyjs="cdn"))
        
    with open(f"AP graphs_both_steps-EU.html", "a") as f:
        f.write(invoices_per_day_by_person_EU_all_graph.to_html(full_html=False, include_plotlyjs="cdn"))
        f.write(invoices_per_month_by_person_EU_all_graph.to_html(full_html=False, include_plotlyjs="cdn"))
        f.write(invoices_per_person_by_month_EU_all_graph.to_html(full_html=False, include_plotlyjs="cdn"))
        f.write(invoices_per_team_by_month_all_graph.to_html(full_html=False, include_plotlyjs="cdn"))




steps = ["Start","Review"]
for step in steps:
    # cleanup(retrieve_data(step), step)
    cleanup(names_correction(team_assignment(load_new_data(step))), step)


frames = get_frames()
transformation_and_graphs(frames)