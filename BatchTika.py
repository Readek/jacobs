import re
import PyPDF2
from tika import parser
import json
import pyodbc
import os
import shutil


class UploadOCR:
    def __init__(self):
        self.OriginalFileLocationID = "C:\\Users\\RG053306\\Documents\\Python\\jacobs\\TestFolder\\Input\\id_retrieval"
        self.OriginalFileLocationSimple = "C:\\Users\\RG053306\\Documents\\Python\\jacobs\\TestFolder\\Input\\simple_split"
        self.FinalFileLocation = (
            "C:\\Users\\RG053306\\Documents\\Python\\jacobs\\TestFolder\\Output"
        )
        self.ArchiveLocation = (
            "C:\\Users\\RG053306\\Documents\\Python\\jacobs\\TestFolder\\Archive"
        )
        self.PendingLocation = (
            "C:\\Users\\RG053306\\Documents\\Python\\jacobs\\TestFolder\\Pending"
        )
        self.TemporaryLocation = (
            "C:\\Users\\RG053306\\Documents\\Python\\jacobs\\TestFolder\\Temp\\"
        )

    def query(self, batchname):
        queryForWaiting = (
            "SELECT DISTINCT invoice_id from correlate.jacobs WHERE batch_name = ?"
        )
        queryForCompare = "SELECT DISTINCT invoice_id from correlate.jacobs_APComparison WHERE batch_name = ?"
        cnxn = pyodbc.connect(
            r"Driver={SQL Server};Server=OAKSQL21;Database=jacobs;Trusted_Connection=yes;"
        )
        cursor = cnxn.cursor()
        cursor.execute(queryForWaiting, os.path.splitext(batchname)[0])
        rows_waiting = cursor.fetchall()
        # print(rows_waiting)
        cursor.execute(queryForCompare, os.path.splitext(batchname)[0])
        rows_compare = cursor.fetchall()
        # print(rows_compare)
        new_rows_waiting = new_rows_compare = []
        if rows_waiting:
            new_rows_waiting = [id for row in rows_waiting for id in row]
        if rows_compare:
            new_rows_compare = [id for row in rows_compare for id in row]
        return [new_rows_waiting, new_rows_compare]

    def BALquery(self, batchfile):
        batchname = os.path.splitext(batchfile)[0]
        query_for_ID = "SELECT INVOICE_ID from correlate.jacobs where batch_name = ? and INVOICE_NO = ?"
        cnxn = pyodbc.connect(
            r"Driver={SQL Server};Server=OAKSQL21;Database=jacobs;Trusted_Connection=yes;"
        )
        cursor = cnxn.cursor()
        for file in os.listdir(
            "C:\\Users\\RG053306\\Documents\\Python\\jacobs\\TestFolder\\Temp\\"
        ):
            # if file.startswith("Check"):
            #     continue
            # else:
            cursor.execute(query_for_ID, (batchname, os.path.splitext(file)[0]))

            invoiceID = cursor.fetchone()
            if invoiceID:
                shutil.move(
                    file, os.path.join(self.FinalFileLocation, f"{invoiceID[0]}-1.pdf")
                )
            else:
                shutil.move(file, f"Please check-{file}")

    def getBALinvoices(self):
        os.chdir(self.PendingLocation)
        regex_page_single = r"Page 1 of 1"
        regex_pages_1 = r"Page 1 of 2"

        for file in sorted(
            os.listdir(self.PendingLocation), key=lambda x: int(os.path.splitext(x)[0]),
        ):
            raw = json.dumps(parser.from_file(file))
            if re.search(regex_page_single, raw):
                os.rename(file, os.path.join(self.TemporaryLocation, f"single{file}",))
            elif re.search(regex_pages_1, raw):
                writer = PyPDF2.PdfFileWriter()
                reader = PyPDF2.PdfFileReader(file)
                writer.appendPagesFromReader(reader)
                reader = PyPDF2.PdfFileReader(
                    f"{int(os.path.splitext(file)[0]) + 1}{os.path.splitext(file)[1]}"
                )
                writer.appendPagesFromReader(reader)
                with open(
                    os.path.join(self.TemporaryLocation, f"single{file}"), "wb"
                ) as output:
                    writer.write(output)

    def BALInvoiceList(self):
        os.chdir(self.TemporaryLocation)
        for file in os.listdir(self.TemporaryLocation):
            raw = json.dumps(parser.from_file(file))
            regex_invoice_number = r"(Invoice # [\d]{7}|\\n[\d]{7})"

            match = re.search(regex_invoice_number, raw)
            if match is None:
                shutil.move(
                    file, f"Check-{file}",
                )
            else:
                shutil.move(file, f"{match[0][-7:]}.pdf")

    def BALErrorCheck(self):
        if len(os.listdir(self.TemporaryLocation)) > 0:
            text = ""
            print(
                f"""Follwing batches require manual check:
{text.join(os.listdir(self.TemporaryLocation))}
Please go to '{self.TemporaryLocation} to fix them.'"""
            )

    def splitEveryPage(self, batchfile, input_location):
        os.chdir(input_location)
        reader = PyPDF2.PdfFileReader(batchfile, strict=False)
        for page in range(reader.getNumPages()):
            writer = PyPDF2.PdfFileWriter()
            new_pdf = writer.addPage(reader.getPage(pageNumber=page))
            output = open(os.path.join(f"{self.PendingLocation}", f"{page}.pdf"), "wb")
            writer.write(output)
            output.close()

    def getIDandpage(self):
        os.chdir(self.PendingLocation)
        regex = r"AUTO ([\d]{7})"
        i = 0
        id_file_list = [[], []]
        for file in sorted(
            os.listdir(self.PendingLocation), key=lambda x: int(os.path.splitext(x)[0]),
        ):
            raw = json.dumps(parser.from_file(file))
            match = re.findall(regex, raw)
            if match:
                id_file_list[0].append(int(file[:-4]))
                id_file_list[1].append(str(match[0]))
        return id_file_list

    def splitAndRename(self, id_file_list, batchfile, input_location):
        os.chdir(input_location)

        reader = PyPDF2.PdfFileReader(batchfile, "rb")
        last = (id_file_list[0][-1], reader.getNumPages())
        zipped = list(zip(id_file_list[0], id_file_list[0][1:]))
        zipped.append(last)
        print(len(zipped))
        for index, item in enumerate(zipped):
            rangeFrom = item[0]
            rangeTo = item[1]
            writer = PyPDF2.PdfFileWriter()
            for page in range(rangeFrom, rangeTo):
                writer.addPage(reader.getPage(page))
            output = open(
                os.path.join(
                    f"{self.FinalFileLocation}", f"{id_file_list[1][index]}-1.pdf"
                ),
                "wb",
            )
            writer.write(output)
            output.close()

    def copyFile(self, rows, batchfile):
        for row in rows:
            shutil.copy2(
                os.path.join(self.OriginalFileLocationSimple, batchfile),
                os.path.join(self.FinalFileLocation, f"{row}-1.pdf"),
            )

    def doublecheck(self, id_list, rows):
        print("doublecheck")
        check1 = [id for id in id_list[1] if id not in rows[1]]
        print(f"List of IDs from file that are not in database: {check1}")
        check2 = [id for id in rows[0] if id not in id_list[1]]
        print(f"List of IDs from database that are not in file: {check2}")

    def cleanup(self):
        os.chdir(self.PendingLocation)
        for file in os.listdir(self.PendingLocation):
            os.remove(file)
        os.chdir(self.TemporaryLocation)

    def archive(self, PathToOriginalFile, ArchiveLocation, input_location):
        os.chdir(input_location)
        shutil.move(PathToOriginalFile, ArchiveLocation)

    def main(self):
        for batchfile in os.listdir(self.OriginalFileLocationID):
            if batchfile == "desktop.ini":
                pass
            elif batchfile.endswith("BAL.pdf"):
                input_location = self.OriginalFileLocationID
                print(batchfile)
                self.splitEveryPage(batchfile, input_location)
                self.getBALinvoices()
                self.BALInvoiceList()
                self.BALquery(batchfile)
                self.archive(batchfile, self.ArchiveLocation, input_location)
                self.cleanup()
            elif "FEDEX" in batchfile or "-SVD" or "ENTERPRISE" in batchfile:
                input_location = self.OriginalFileLocationID
                print(batchfile)
                self.splitEveryPage(batchfile, input_location)
                rows = self.query(batchfile)
                id_list = self.getIDandpage()
                self.splitAndRename(id_list, batchfile, input_location)
                self.doublecheck(id_list, rows)
                self.archive(batchfile, self.ArchiveLocation, input_location)
                self.cleanup()
        for batchfile in os.listdir(self.OriginalFileLocationSimple):
            input_location = self.OriginalFileLocationSimple
            if batchfile == "desktop.ini":
                pass
            else:
                try:
                    print(batchfile)
                    rows = self.query(batchfile)[0]
                    if rows == []:
                        print("No data in database, please verify")
                    else:
                        self.copyFile(rows, batchfile)
                        self.archive(batchfile, self.ArchiveLocation, input_location)
                except Exception:
                    print("No data in database, please verify")
                    print(Exception)
        self.BALErrorCheck()


if __name__ == "__main__":
    objName = UploadOCR()
    objName.main()
    input()
